const initialState = () => ({
  user: undefined,
  height: undefined,
  weight: undefined,
  date: undefined,
  gender: undefined,
  boy: undefined,
  girl: undefined,
  email: undefined
})

export const state = initialState

// export const state = () => ({
//   user: undefined,
//   height: '5',
//   weight: '2',
//   date: '3',
//   gender: '1',
//   boy: 'Z',
//   girl: 'E',
//   email: 'a@l.ok'
// })

export const actions = {
  reset(store) {
    store.commit('setVotes', initialState())
  }
}

export const mutations = {
  setVote(state, vote) {
    state[vote.key] = vote.value
  },
  setVotes(state, votes) {
    for (const vote in votes) {
      state[vote] = votes[vote]
    }
  }
}

export const getters = {
  allVotes(state) {
    return state
  }
}
